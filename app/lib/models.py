#!/usr/bin/python 
# -*- coding: utf-8 -*-

import uuid
from django.db import models
from django.contrib.contenttypes.models import ContentType

class TrackingMixin(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    cdate = models.DateTimeField(auto_now_add=True, verbose_name=u'Creación')
    udate = models.DateTimeField(auto_now=True, verbose_name=u'Actualización')

    class Meta:
        abstract = True


class AccountMixin(object):
    _name_account = None

    def get_account(self):
        from transacciones.models import Account
        try:
            ct = ContentType.objects.get_for_model(self)
            account = Account.objects.get(content_type=ct, object_id=self.id)
        except Account.DoesNotExist:
            account = Account.objects.create(content_object=self)
        return account

    def get_account_name(self):
        if not self._name_account:
            account = self.get_account()
            self._name_account = account.name
        return self._name_account
