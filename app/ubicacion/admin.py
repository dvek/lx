from django.contrib import admin

from models import State, City, Branch, Shop


@admin.register(State)
class StateAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug': ('name', )}


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug': ('name', )}


@admin.register(Branch)
class BranchAdmin(admin.ModelAdmin):
	pass


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
	pass
