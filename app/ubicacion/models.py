#!/usr/bin/python 
# -*- coding: utf-8 -*-

from django.db import models
from lib.models import TrackingMixin, AccountMixin


class State(TrackingMixin):
    """
    representacion del departamento politico
    """
    name = models.CharField(max_length=150, verbose_name=u'Departamento')
    slug = models.SlugField(null=True, blank=True)

    class Meta:
        verbose_name = u'Departamento'
        verbose_name_plural = u'Departamentos'

    def __unicode__(self):
        return u'%s' % self.name


class City(TrackingMixin):
    """
    ciudad de la sucursal u oficina
    """
    name = models.CharField(max_length=200, verbose_name=u'Ciudad')
    slug =  models.SlugField(null=True, blank=True)

    class Meta:
        verbose_name = u'Ciudad'
        verbose_name_plural = u'Ciudades'

    def __unicode__(self):
        return u'%s' % self.name


class Branch(TrackingMixin):
    """
    representacion de la sucursal
    """
    name = models.CharField(max_length=150, verbose_name=u'Sucursal')

    class Meta:
        verbose_name = u'Sucursal'
        verbose_name_plural = u'Sucursales'

    def __unicode__(self):
        return u'%s' % self.name


class Shop(TrackingMixin, AccountMixin):
    """
    representacion del almacen/tienda
    """
    name = models.CharField(max_length=200, verbose_name=u'Tienda/Almacen')
    state = models.ForeignKey(State, verbose_name=u'Departamento')
    city = models.ForeignKey(City, verbose_name=u'Ciudad')
    location = models.TextField(verbose_name=u'Ubicacion')
    phone = models.CharField(max_length=20, verbose_name=u'Telefono')

    class Meta:
        verbose_name = u'Tienda'
        verbose_name_plural = u'Tiendas'

    def __unicode__(self):
        return u'%s' % self.name