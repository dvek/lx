from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from models import UpdateStock


@receiver(post_save, sender=UpdateStock)
def update_flows(sender, instance, **kwargs):
    """
    actualizacion de los inventarios
    """
    pass