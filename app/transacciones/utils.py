from django.db import transaction
from django.contrib.contenttypes.models import ContentType
from models import Account, Transacction, Input, DiscountBonus

class TransacctionException(Exception):
    pass

@transaction.atomic
def create_transacction(user, description, invoice, inputs=[]):
    """
    create transacction for store account and credit/debit
    """
    if not inputs:
        return None

    if not sum(line['amount'] for line in inputs) == 0:
        raise TransacctionException('Error on validate transacctions. The sum is not equal zero!')

    transaction = Transacction.objects.create(
        user=user,
        comment=description,
        invoice=invoice)

    # TODO revisar al guardado de transacciones
    for line in inputs:
        transaction.inputs.create(
            account=line['account'],
            amount=line['amount'])

    return transaction

def find_discount(amount):
    try:
        discount = DiscountBonus.objects.get(
            lower_limit__lte=amount, 
            upper_limit__gte=amount
        ).discount
    except DiscountBonus.DoesNotExist:
        discount = 0
    return discount