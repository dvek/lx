# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('transacciones', '0003_auto_20151104_2033'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='account',
            name='client',
        ),
        migrations.RemoveField(
            model_name='input',
            name='quantity',
        ),
        migrations.AddField(
            model_name='account',
            name='content_type',
            field=models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='account',
            name='object_id',
            field=models.CharField(max_length=64, null=True),
        ),
        migrations.AddField(
            model_name='input',
            name='amount',
            field=models.DecimalField(default=0, verbose_name='Monto', max_digits=12, decimal_places=4),
            preserve_default=False,
        ),
    ]
