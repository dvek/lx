# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('clientes', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('facturas', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255, verbose_name='Nombre cuenta')),
                ('balance', models.DecimalField(verbose_name='Balance', max_digits=8, decimal_places=2)),
                ('client', models.OneToOneField(verbose_name='Cliente', to='clientes.Client')),
            ],
            options={
                'verbose_name': 'Cuenta',
                'verbose_name_plural': 'Cuentas',
            },
        ),
        migrations.CreateModel(
            name='DiscountBonus',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('upper_limit', models.DecimalField(verbose_name='Monto superior', max_digits=12, decimal_places=4)),
                ('lower_limit', models.DecimalField(verbose_name='Monto inferior', max_digits=12, decimal_places=4)),
                ('discount', models.DecimalField(help_text='En porcentaje sobre la venta', verbose_name='Descuento', max_digits=12, decimal_places=4)),
                ('bonus', models.DecimalField(help_text='En porcentaje sobre la venta', verbose_name='Premio', max_digits=12, decimal_places=4)),
            ],
            options={
                'verbose_name': 'Descuento y premio',
                'verbose_name_plural': 'Descuentos y premios',
            },
        ),
        migrations.CreateModel(
            name='Input',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('quantity', models.DecimalField(verbose_name='Cantidad', max_digits=12, decimal_places=4)),
                ('account', models.ForeignKey(related_name='inputs', verbose_name='Cuenta', to='transacciones.Account')),
            ],
            options={
                'verbose_name': 'Entrada',
                'verbose_name_plural': 'Entradas',
            },
        ),
        migrations.CreateModel(
            name='Transacction',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('comment', models.CharField(max_length=1000)),
                ('invoice', models.ForeignKey(related_name='transacctions', verbose_name='Factura', blank=True, to='facturas.Invoice', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Transaccion',
                'verbose_name_plural': 'Transacciones',
            },
        ),
        migrations.AddField(
            model_name='input',
            name='transacction',
            field=models.ForeignKey(related_name='inputs', verbose_name='Transaccion', to='transacciones.Transacction'),
        ),
    ]
