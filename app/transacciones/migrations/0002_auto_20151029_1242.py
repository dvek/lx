# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transacciones', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='account',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='discountbonus',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='discountbonus',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='input',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='input',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='transacction',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='transacction',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
    ]
