# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transacciones', '0002_auto_20151029_1242'),
    ]

    operations = [
        migrations.AlterField(
            model_name='input',
            name='account',
            field=models.CharField(max_length=255, null=True, verbose_name='Cuenta'),
        ),
        migrations.AlterField(
            model_name='input',
            name='quantity',
            field=models.DecimalField(verbose_name='Cantidad $', max_digits=12, decimal_places=4),
        ),
    ]
