from django.contrib import admin
from models import Account, Transacction, Input, DiscountBonus


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('name', 'balance', 'content_object')

    def get_readonly_fields(self, request, obj=None):
        return ('name', 'balance', 'content_object')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class InputInLineAdmin(admin.TabularInline):
    model = Input
    fields = ('account', 'amount')

    def get_readonly_fields(self, request, obj=None):
        """
        editable fields on new and edit with revision status
        """
        return ('account', 'amount')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Transacction)
class TransacctionAdmin(admin.ModelAdmin):
    list_display = ('cdate', 'user', 'comment', 'invoice')
    inlines = [InputInLineAdmin]


    def get_readonly_fields(self, request, obj=None):
        return ('cdate', 'user', 'comment', 'invoice')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(DiscountBonus)
class DiscountBonusAdmin(admin.ModelAdmin):
    list_display = ('lower_limit', 'upper_limit', 'discount', 'bonus')
