#!/usr/bin/python 
# -*- coding: utf-8 -*-

import string
from django.db import models
from django.db.models import Avg, Sum, Max, Min
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.utils import timezone
from django.utils.timezone import timedelta
from django.template.defaultfilters import slugify
from django.conf import settings

from lib.models import TrackingMixin
from managers import TransacctionManager


class Account(TrackingMixin):

    name = models.CharField(max_length=255, verbose_name=u'Nombre cuenta')
    balance = models.DecimalField(max_digits=8, decimal_places=2, default=0, verbose_name=u'Balance')

    content_type = models.ForeignKey(ContentType, null=True, blank=True)
    object_id = models.CharField(max_length=64, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = u'Cuenta'
        verbose_name_plural = u'Cuentas'

    def __unicode__(self):
        return u'%s' % self.name

    def save(self, *args, **kwargs):
        if not self.name:
            self.name = self.make_name(str(self.content_object))
        super(Account, self).save(*args, **kwargs)

    def _balance(self):
        total = self.inputs.aggregate(total=Sum('quantity'))['total']
        return Decimal('0.0000') if total is None else total

    def make_name(self, name):
        """
        convert to adecuate account name like:
        store.products.swear.red
        """
        account_name = string.replace(slugify(name), '-', '')
        return "{0}.{1}.{2}".format('lhex', str(self.content_type), account_name)
        

class Transacction(TrackingMixin):

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    comment = models.CharField(max_length=1000)
    invoice = models.ForeignKey(
        'facturas.Invoice', 
        related_name='transacctions',
        null=True, blank=True, verbose_name=u'Factura')
    
    class Meta:
        verbose_name = u'Transaccion'
        verbose_name_plural = u'Transacciones'
        ordering = ['-cdate']

    def __unicode__(self):
        return u'%s' % self.id

    def print_transaction(self):
        pass
    

class Input(TrackingMixin):

    transacction = models.ForeignKey(Transacction, related_name='inputs', verbose_name=u'Transaccion')
    account = models.CharField(max_length=255, null=True, verbose_name=u'Cuenta')
    amount = models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Monto')
    
    class Meta:
        verbose_name = u'Entrada'
        verbose_name_plural = u'Entradas'

    def __unicode__(self):
        return u'%s' % self.id


class DiscountBonus(TrackingMixin):
    upper_limit = models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Monto superior')
    lower_limit = models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Monto inferior')
    discount = models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Descuento',
        help_text=u'En porcentaje sobre la venta')
    bonus =  models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Premio',
        help_text=u'En porcentaje sobre la venta')
    
    class Meta:
        verbose_name = u'Descuento y premio'
        verbose_name_plural = u'Descuentos y premios'

    def __unicode__(self):
        return u'Range: (%s, %s), Discount: %s, Bonus: %s' % (
            self.lower_limit, 
            self.upper_limit,
            self.discount, 
            self.bonus)

