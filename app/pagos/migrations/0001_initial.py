# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('facturas', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('amount', models.DecimalField(verbose_name=b'Cantidad', max_digits=12, decimal_places=4)),
                ('detail', models.CharField(max_length=255, null=True, verbose_name=b'Detalle', blank=True)),
                ('reference', models.CharField(max_length=255, null=True, blank=True)),
                ('invoice', models.ForeignKey(related_name='payments', to='facturas.Invoice')),
            ],
            options={
                'verbose_name': 'Pago',
                'verbose_name_plural': 'Pagos',
            },
        ),
    ]
