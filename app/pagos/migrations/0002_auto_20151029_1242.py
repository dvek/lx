# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pagos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
    ]
