#!/usr/bin/python 
# -*- coding: utf-8 -*-

import string
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.template.defaultfilters import slugify

from lib.models import TrackingMixin, AccountMixin


class Payment(TrackingMixin, AccountMixin):

    amount = models.DecimalField(max_digits=12, decimal_places=4, verbose_name='Cantidad')
    detail = models.CharField(max_length=255, blank=True, null=True, verbose_name='Detalle')
    reference = models.CharField(max_length=255,
                                 blank=True,
                                 null=True)
    invoice = models.ForeignKey('facturas.Invoice', related_name='payments')

    class Meta:
        verbose_name = 'Pago'
        verbose_name_plural = 'Pagos'
        ordering = ['cdate']

    def __unicode__(self):
        return u'%s' % self.invoice.client

    def save(self, *args, **kwargs):
        #if not self.reference:
        #    self.reference = self._make_reference(str(self.invoice.client))
        super(Payment, self).save(*args, **kwargs)

    def _make_reference(self, name):
        """
        convert to adecuate account name like:
        store.products.swear.red
        """
        account_name = string.replace(slugify(name), '-', '')
        return "{0}.{1}.{2}".format('lhex', 'payments', account_name)
