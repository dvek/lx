from decimal import Decimal
from django.db.models.signals import post_save, pre_save
from django.db.models import Avg, Sum, Max, Min
from django.dispatch import receiver
from django.utils import timezone
from django.utils.timezone import timedelta

from models import Payment


@receiver(post_save, sender=Payment)
def descuento_estimado(sender, instance, **kwargs):
    invoice = instance.invoice
    payments = Payments.objects.filter(invoice=invoice).aggregate(total=Sum('amount'))['total']
    if invoice.total() >= payments:
        invoice.active = False
        invoice.save()

        

        
        