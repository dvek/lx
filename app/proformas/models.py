# #!/usr/bin/python 
# # -*- coding: utf-8 -*-

# import uuid
# from django.conf import settings
# from django.db import models
# from lib.models import TrackingMixin


# class Proforma(TrackingMixin):

#     id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
#     pend = models.BooleanField(default=True, verbose_name=u'Pendiente')
#     offer = models.BooleanField(default=False, verbose_name=u'Oferta')
#     client = models.ForeignKey('clientes.Client', verbose_name=u'Cliente')
#     user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Usuario')
#     description = models.TextField(max_length=300, 
#         null=True, blank=True, verbose_name=u'Descripcion')
#     discount = models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Descuento')
#     term = models.DateField(blank=True, null=True, verbose_name=u'Vencimiento')
#     invoice = models.ForeignKey('facturas.Invoice', null=True, verbose_name=u'Factura')

#     class Meta:
#         verbose_name = u'Proforma de venta'
#         verbose_name_plural = u'Proformas de ventas'

#     def __unicode__(self):
#         return u'%s' % self.id


# class ArticleProforma(TrackingMixin):

#     id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
#     proforma = models.ForeignKey(Proforma, verbose_name=u'Proforma')
#     article = models.ForeignKey('productos.Article', verbose_name=u'Producto')
#     quantity = models.IntegerField(default=0, verbose_name=u'Cantidad')







