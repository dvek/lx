from fabric.api import *
from fabric.colors import green, red

def server():
    env.host_string = 'lhexagone.hanantek.com'
    env.user = 'admin'
    env.password= 'sesam0.1'

def deployment():
    path = '/home/admin'
    process = 'lx'

    print(red('Beginning deploy:'))
    with cd('%s/lx' % path):
        run('pwd')
        print(green('Pulling master from server...'))
        run('hg up')
        print(green('Installing requirements...'))
        run('source %s/lxenv/bin/activate && pip install -r requirements.txt' % path)
        print(green('Collecting static files...'))
        run('source %s/lxenv/bin/activate && python manage.py collectstatic --noinput' % path)
        print(green('Migrating the dababase...'))
        run('source %s/lxenv/bin/activate && python manage.py migrate' % path)
        print(green('Populate the database...'))
        run('source %s/lexve/bin/activate && python manage.py populate' % path)
        print(green('Restart server'))
        sudo('supervisorctl restart %s' % process)
        
    print(red('DONE!'))