#!/usr/bin/python 
# -*- coding: utf-8 -*-

from decimal import Decimal
from django.db import models
from django.db.models import Avg, Sum, Max, Min
from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.utils import timezone
from django.utils.timezone import timedelta
from django.conf import settings


from lib.models import TrackingMixin
from managers import InvoiceManager, ProformaManager
from transacciones.utils import find_discount


class Proforma(TrackingMixin):
    """
    representacion para las proformas
    """
    STATUS = [
        ('revision', u'En revisión'),
        ('approved', u'Aprobada')
    ]

    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Usuario')
    shop = models.ForeignKey('ubicacion.Shop', null=True, verbose_name='Almacen/Tienda de origen')
    description = models.TextField(max_length=300, 
        null=True, blank=True, verbose_name=u'Descripcion')
    client = models.ForeignKey('clientes.Client', null=True, verbose_name=u'Cliente')
    discount = models.DecimalField(max_digits=12, default='0.0000', blank=True, decimal_places=4, verbose_name=u'Descuento')
    status = models.CharField(max_length=15, default='revision', choices=STATUS, verbose_name=u'Estado')

    objects = ProformaManager()

    class Meta:
        verbose_name = u'Proforma'
        verbose_name_plural = u'Proformas'

    def __unicode__(self):
        return u'Proforma %s' % self.id

    @property
    def invoice(self):
        if hasattr(self, 'invoice_lines'):
            return self.invoice_lines.all().first().invoice.id
        return '- Ninguna -'

    @property
    def total(self):
        total  = Decimal('0.00')
        for lines in self.invoice_lines.all():
            total += lines.total()
        return total

    @property
    def quantities(self):
        total = 0
        for lines in self.invoice_lines.all():
            total += lines.quantity
        return total

    def close_date(self):
        expire = self.cdate + timedelta(days=settings.EXPIRE_PROFORMA)
        return expire
    close_date.short_description = u'Fecha cierre'

    @property
    def _expire(self):
        return (self.close_date() < timezone.now())

    def expire(self):
        if self.status == 'revision': 
            return self.close_date() if not self._expire else '<span style="color: red">Expiro</span>'
        return '--'
    expire.allow_tags = True
    expire.short_description = u'Expira'

class Invoice(TrackingMixin):
    """
    representacion de la facura
    """
    client = models.ForeignKey('clientes.Client', null=True, verbose_name=u'Cliente')
    description = models.TextField(max_length=300, 
        null=True, blank=True, verbose_name=u'Descripcion')
    #discount = models.DecimalField(max_digits=12, default='0.0000', blank=True, decimal_places=4, verbose_name=u'Descuento')
    active = models.BooleanField(default=True, verbose_name='Activa')

    objects = InvoiceManager()

    class Meta:
        verbose_name = u'Factura'
        verbose_name_plural = u'Facturas'

    def __unicode__(self):
        return u'%s cliente: %s' % (self.id, self.client if self.client else '- Ninguno -')

    def close_date(self):
        expire = self.cdate + timedelta(days=settings.EXPIRE_INVOICE)
        return expire
    close_date.short_description = u'Fecha cierre'

    @property
    def _expire(self):
        return (self.close_date() < timezone.now())

    def expire(self):
        if self.active is True:
            return self.close_date() if not self._expire else '<span style="color: red">Expiro</span>'
        return '--'
    expire.allow_tags = True
    expire.short_description = u'Expira'

    @property
    def total(self):
        total  = Decimal('0.00')
        for lines in self.lines.all():
            total += lines.total()
        return total
    
    def total_payments(self):
        total = self.payments.aggregate(total=Sum('amount'))['total']
        if total is not None:
            return total
        return 0
    total_payments.short_description = u'Pagos'

    def total_discounts(self):
        if not self._expire:
            if not hasattr(self, '_discounts'):
                self._discounts = self._calculate_discount()
            return self._discounts
        return 0
    total_discounts.short_description = u'Descuentos'

    def total_debt(self):
        return self.total - self.total_discounts() - self.total_payments()
    total_debt.short_description = u'Saldo a pagar'

    def _calculate_discount(self):
        percent_discount = find_discount(self.total)
        return self.total * percent_discount/100

    def has_open_prof(self):
        """
        check if invoice have any proforma with status in 'revision'
        """
        return Proforma.objects.filter(invoice_lines__in=self.lines.all()).filter(status='revision').exists()


class InvoiceLine(TrackingMixin):
    """
    representacion de los productos y su informacion relacionada de la factura
    """
    invoice = models.ForeignKey(Invoice, verbose_name=u'Factura', related_name='lines')
    proforma = models.ForeignKey(Proforma, verbose_name=u'Proforma', related_name='invoice_lines')
    article = models.ForeignKey('productos.Article', verbose_name=u'Producto')
    quantity = models.IntegerField(verbose_name=u'Cantidad', default=0)

    class Meta:
        verbose_name = u'Articulo factura'
        verbose_name_plural = u'Articulos factura'

    def __unicode__(self):
        return u'%s %s' % (self.article, self.quantity)

    def available(self):
        return self.article.qty_stock
    available.short_description = u'Disponible'

    def price(self):
        # cliente del tipo credito => precios de distribuidor 
        if self.proforma.client.client_type.credit == True:
            return self.article.dist_price
        else:
            return self.article.public_price
    price.short_description = u'Precio'

    def total(self):
        return Decimal(self.price() * self.quantity)
    total.short_description = u'Total'

