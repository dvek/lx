from django.contrib import admin, messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.conf.urls import patterns, include, url
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.contenttypes.models import ContentType

from models import Proforma, Invoice, InvoiceLine
from utils import update_proforma, pay_invoice
from forms import ProformaForm, ProformaInvoiceLineForm
from seguridad.utils import has_su_perm, has_admin_perm, has_user_perm
from inventarios.models import Flow
from transacciones.models import Transacction
from pagos.models import Payment


class InvoiceLineAdmin(admin.TabularInline):
    model = InvoiceLine
    fields = ('proforma', 'article', 'quantity', 'price', 'total')

    def get_readonly_fields(self, request, obj=None):
        """
        editable fields on new and edit with revision status
        """
        return ('proforma', 'article', 'quantity', 'price', 'total')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class NewProformaInvoiceLineAdmin(admin.TabularInline):
    model = InvoiceLine
    fields = ('article', 'quantity', 'available', 'price', 'total')
    readonly_fields = ('available', 'price', 'total')
    formset = ProformaInvoiceLineForm

    raw_id_fields = ('article',)
    autocomplete_lookup_fields = {
        'fk': ['article'],
    }


class ProformaInvoiceLineAdmin(admin.TabularInline):
    model = InvoiceLine
    fields = ('article', 'quantity', 'available', 'price', 'total')
    formset = ProformaInvoiceLineForm

    raw_id_fields = ('article',)
    autocomplete_lookup_fields = {
        'fk': ['article'],
    }

    def get_readonly_fields(self, request, obj=None):
        fields = ('price', 'total' , 'available')
        if obj and obj.status == 'revision' and obj._expire:
            return fields + ('article', 'quantity')
        return fields

    def has_add_permission(self, request, obj=None):
        return False



class ReadProformaInvoiceLineAdmin(admin.TabularInline):
    """
    read only line items when status is approved
    """
    model = InvoiceLine
    fields = ('article', 'quantity', 'available', 'price', 'total')
    readonly_fields = ('article', 'quantity', 'available', 'price', 'total')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Proforma)
class ProformaAdmin(admin.ModelAdmin):
    form = ProformaForm
    list_display = ('id', 'client', 'discount', 'cdate', 'udate', 'expire', 'user', 'status', 'total')

    raw_id_fields = ('client',)
    autocomplete_lookup_fields = {
        'fk': ['client'],
    }

    class Media:
        js = (
            'js/proformas.js',
        )

    def get_fields(self, request, obj=None):
        fields = ('client', 'shop', 'description', 'discount',)
        if obj and obj.id:
            if has_su_perm(request.user):
                return fields + ('status',)
            elif has_user_perm(request.user):
                return fields
        return fields

    def get_readonly_fields(self, request, obj=None):
        """
        editable fields on new and edit with revision status
        """
        fields = ('client', 'shop', 'description', 'discount', 'status')
        if obj:
            return fields
        return []

    def get_form(self, request, obj=None, **kwargs):
        """
        return editable or readonly inline form if
        status is revision
        """
        if not obj:
            self.inlines = [NewProformaInvoiceLineAdmin]
        if obj and obj.id:
            if obj.status == 'revision' and obj._expire:
                messages.error(request, 'Esta proforma caduco, se requiere aprobacion para cerrarla')
            self.inlines = [ProformaInvoiceLineAdmin]
        if obj and obj.id and obj.status == 'approved':
            self.inlines = [ReadProformaInvoiceLineAdmin]

        return super(ProformaAdmin, self).get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
        """
        dont remove this method for after save parent and lines 
        """
        if obj and obj.id:
            if not has_su_perm(request.user):
                obj.status = 'approved'
        pass

    def save_formset(self, request, form, formset, change):
        form.instance.user = request.user       
        instances = formset.save(commit=False)
        update_proforma(form.instance, instances)
        
    def add_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = True
        return super(ProformaAdmin, self).add_view(
            request=request, 
            extra_context=extra_context)

    def change_view(self, request, object_id, extra_context=None):
        approved = (self.model.objects.get(id=object_id).status == 'approved')
        extra_context = extra_context or {}
        extra_context['show_print_client'] = True
        extra_context['show_print_public'] = True 
        if has_su_perm(request.user) and not approved:
            extra_context['show_save'] = True
            extra_context['show_approve'] = True
        if has_admin_perm(request.user) and not approved:
            extra_context['show_approve'] = True
        return super(ProformaAdmin, self).change_view(
            request=request, 
            object_id=object_id,
            extra_context=extra_context)

    def response_change(self, request, obj):
        """
        get from response change some custom action from post
        ej: '_custom_action' in request.POST:
        """
        if '_approve' in request.POST:
            obj.status = 'approved'
            obj.save()
        return super(ProformaAdmin, self).response_change(request, obj)

    def has_delete_permission(self, request, obj=None):
        if obj and obj.id:
            return False
        return True

    def get_urls(self):
        urls = super(ProformaAdmin, self).get_urls()
        other_urls = patterns('',
            (r'^(?P<uuid>[^/]+)/imprimir/publico/', self.imprimir_publico),
            (r'^(?P<uuid>[^/]+)/imprimir/cliente/', self.imprimir_cliente),
        )
        return other_urls + urls

    def imprimir_publico(self, request, uuid):
        return render_to_response(
            'admin/facturas/proforma/impresion_publico.html',
            {
                'proforma': Proforma.objects.get(id=uuid),
                'productos': InvoiceLine.objects.filter(proforma__id=uuid),
                'devueltos': Flow.objects.filter(
                    object_id=uuid,
                    content_type=ContentType.objects.get_for_model(Proforma),
                    quantity__lte=0, 
                    client__isnull=False),
            },
            context_instance=RequestContext(request)
        )

    def imprimir_cliente(self, request, uuid):
        return render_to_response(
            'admin/facturas/proforma/impresion_distribuidor.html',
            {
                'proforma': Proforma.objects.get(id=uuid),
                'productos': InvoiceLine.objects.filter(proforma__id=uuid),
                'devueltos': Flow.objects.filter(
                    object_id=uuid,
                    content_type=ContentType.objects.get_for_model(Proforma),
                    quantity__lte=0, 
                    client__isnull=False),
            },
            context_instance=RequestContext(request)
        )

class PaymentAdminLine(admin.TabularInline):
    model = Payment
    fields = ('amount', 'detail')
    extra = 1
    verbose_name_plural = u'Nuevo Pago'

    def get_queryset(self, request):
        return Payment.objects.none()

    def has_delete_permission(self, request, obj=None):
        return False

    #def has_add_permission(self, request, obj=None):
    #    return False


class PaymentReadAdminLine(admin.TabularInline):
    model = Payment
    fields = ('cdate', 'amount', 'detail', 'reference')
    readonly_fields = ('cdate', 'amount', 'detail', 'reference')
    verbose_name_plural = u'Pagos realizados'

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    #inlines = [InvoiceLineAdmin, PaymentReadAdminLine, PaymentAdminLine]
    list_display = ('id', 'client', 'description', 'cdate', 'udate', 'expire', 
        'active', 'total', 'total_discounts', 'total_payments', 'total_debt')

    def get_readonly_fields(self, request, obj=None):
        return ('client', 'description', 'active', 'total', 
            'total_discounts', 'total_payments', 'total_debt')

    def get_form(self, request, obj=None, **kwargs):
        if obj and obj.id:
            self.inlines = [InvoiceLineAdmin, PaymentReadAdminLine, PaymentAdminLine]
        if obj and obj.id and obj.active is False:
            self.inlines = [InvoiceLineAdmin, PaymentReadAdminLine]

        return super(InvoiceAdmin, self).get_form(request, obj, **kwargs)

    def change_view(self, request, object_id, extra_context=None):
        """
        set some flags for view buttons (save)
        """
        extra_context = extra_context or {}
        obj = self.model.objects.get(id=object_id)
        
        if obj.active is True and obj._expire:
            messages.error(request, 'Esta factura ha caducado, se requiere pagarla para cerrarla')

        extra_context['show_save'] = obj.active        
        if obj.active and obj.has_open_prof():
            messages.error(request, 'Esta factura aun tiene proformas sin aprovar')
            extra_context['show_save'] = False

        return super(InvoiceAdmin, self).change_view(
            request=request, 
            object_id=object_id,
            extra_context=extra_context)

    def save_model(self, request, obj, form, change):
        """
        dont remove this method for after save parent and lines 
        """
        pass

    def save_formset(self, request, form, formset, change):
        # for payments instances
        instances = formset.save(commit=False)
        pay_invoice(invoice=form.instance, user=request.user, payment_list=instances)

