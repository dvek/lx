if (!$) {
    $ = django.jQuery;
}

$(document).ready(function() {
    console.log('ready');

    var articulos = {};

    // calcula el total en base a la cantidad
    $('input[id$="quantity"]').blur(function(e) {
        var index = $(this).attr("id").split('-')[1];
        var cantidad = $(this).val();
        var precio = $('#invoice_lines'+index).children('div').children('div.grp-td.price').children('div').text();
        
        $('#invoice_lines'+index).children('div')
            .children('div.grp-td.total').children('div').text(precio * cantidad);


        $('#invoice_lines'+index).children('div').
            children('div.grp-td.available').children('div').text(articulos[index] - cantidad);
        
    });

    $('input[id$="article"]').blur(function(e) {
        console.log('autocomplete...');
        var v = $(this).val();
        console.log(v);
        var index = $(this).attr("id").split('-')[1];
        var cantidad = $('#id_proformas-'+index+'-cantidad').val();
        var cliente = $('#id_cliente').val();

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/api/1.0/products/"+v,
            success: function(data) {
                var precio = 0;
                console.log(data);
                if(data.qty_stock <= 0) {
                    alert('Cantidad en stock no disponible para este producto');
                    $('#id_invoices-'+index+'-quantity').attr('readonly', true);
                } else {
                    if(window.credito) {
                        precio = parseFloat(data.dist_price);
                    } else {
                        precio = parseFloat(data.public_price);
                    }
                }
                articulos[index] = data.qty_stock;
                cantidad = $('#id_invoice_lines-'+index+'-quantity').val();
                $('#invoice_lines'+index).children('div').children('div.grp-td.price').children('div').text(precio);
                $('#invoice_lines'+index).children('div')
                    .children('div.grp-td.available').children('div').text(articulos[index]);
                $('#invoice_lines'+index).children('div')
                    .children('div.grp-td.total').children('div').text(precio * cantidad);
            },
            error: function(msg, url, line) {
                console.log(msg.statusText);
            }
        });

    });
});