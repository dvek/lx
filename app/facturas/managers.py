from django.db import models

class InvoiceManager(models.Manager):

    def get_queryset(self):
        return super(InvoiceManager, self).get_queryset().order_by('-cdate')
    
    def actives(self):
        qs = self.get_queryset()
        return qs.filter(active=True).distinct()


class ProformaManager(models.Manager):

    def get_queryset(self):
        return super(ProformaManager, self).get_queryset().order_by('-cdate')
