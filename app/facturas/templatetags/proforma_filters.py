from django import template

register = template.Library()

@register.filter(vale='absolute')
def absolute(value):
    if value > 0:
        return value
    else:
        return -value