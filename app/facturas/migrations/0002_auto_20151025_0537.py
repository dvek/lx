# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('facturas', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='invoiceline',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='invoiceline',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='proforma',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='proforma',
            name='discount',
            field=models.DecimalField(default=b'0.0000', verbose_name='Descuento', max_digits=12, decimal_places=4, blank=True),
        ),
        migrations.AlterField(
            model_name='proforma',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
    ]
