# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ubicacion', '0002_auto_20151026_0115'),
        ('facturas', '0003_proforma_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='proforma',
            name='shop',
            field=models.ForeignKey(verbose_name=b'Almacen/Tienda de origen', to='ubicacion.Shop', null=True),
        ),
        migrations.AlterField(
            model_name='invoiceline',
            name='quantity',
            field=models.IntegerField(default=0, verbose_name='Cantidad'),
        ),
        migrations.AlterField(
            model_name='proforma',
            name='status',
            field=models.CharField(default=b'revision', max_length=15, verbose_name='Estado', choices=[(b'revision', 'En revisi\xf3n'), (b'approved', 'Aprobada')]),
        ),
    ]
