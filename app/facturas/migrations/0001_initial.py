# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('productos', '0001_initial'),
        ('clientes', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('description', models.TextField(max_length=300, null=True, verbose_name='Descripcion', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name=b'Activa')),
                ('client', models.ForeignKey(verbose_name='Cliente', to='clientes.Client', null=True)),
            ],
            options={
                'verbose_name': 'Factura',
                'verbose_name_plural': 'Facturas',
            },
        ),
        migrations.CreateModel(
            name='InvoiceLine',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('quantity', models.IntegerField(verbose_name='Cantidad')),
                ('article', models.ForeignKey(verbose_name='Producto', to='productos.Article')),
                ('invoice', models.ForeignKey(verbose_name='Factura', to='facturas.Invoice')),
            ],
            options={
                'verbose_name': 'Articulo factura',
                'verbose_name_plural': 'Articulos factura',
            },
        ),
        migrations.CreateModel(
            name='Proforma',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('description', models.TextField(max_length=300, null=True, verbose_name='Descripcion', blank=True)),
                ('discount', models.DecimalField(null=True, verbose_name='Descuento', max_digits=12, decimal_places=4, blank=True)),
                ('client', models.ForeignKey(verbose_name='Cliente', to='clientes.Client', null=True)),
                ('user', models.ForeignKey(verbose_name='Usuario', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Proforma',
                'verbose_name_plural': 'Proformas',
            },
        ),
        migrations.AddField(
            model_name='invoiceline',
            name='proforma',
            field=models.ForeignKey(related_name='invoice_lines', verbose_name='Proforma', to='facturas.Proforma'),
        ),
    ]
