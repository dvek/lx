# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('facturas', '0002_auto_20151025_0537'),
    ]

    operations = [
        migrations.AddField(
            model_name='proforma',
            name='status',
            field=models.CharField(default=b'revision', max_length=15, choices=[(b'En revision', b'revision'), (b'Aprobada', b'approved')]),
        ),
    ]
