from django import forms
from models import Proforma


class ProformaNewForm(forms.ModelForm):
    extra_field = forms.CharField()

    def clean(self, *args, **kwargs):
        super(ProformaNewForm, self).clean(*args, **kwargs)

        for form in self.forms:
            if not form.is_valid():
                raise ValidationError("Corrija sus datos de la lista de productos")


class ProformaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProformaForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            pass

    def clean(self, *args, **kwargs):
        instance = getattr(self, 'instance', None)
        super(ProformaForm, self).clean(*args, **kwargs)


class ProformaInvoiceLineForm(forms.models.BaseInlineFormSet):

    def clean(self, *args, **kwargs):
        count = 0
        for form in self.forms:
            try:
                if form.cleaned_data:
                    count += 1
            except AttributeError:
                pass
        if count < 1:
            raise forms.ValidationError('Debe tener por lo menos un producto comprado')
        super(ProformaInvoiceLineForm, self).clean(*args, **kwargs)

