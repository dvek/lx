from django.apps import AppConfig


class InvoiceConfig(AppConfig):
    name = 'invoices'

    def ready(self):
        pass