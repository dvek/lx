from django.db import transaction
from models import Proforma, Invoice, InvoiceLine
from inventarios.utils import transfer_flow
from transacciones.utils import create_transacction

@transaction.atomic
def update_proforma(proforma, line_items):
    """
    create invoice first -> items -> proforma
    """
    
    tr_list = []

    actives = Invoice.objects.actives().filter(client=proforma.client)
    if len(actives) == 1:
        # update invoice
        invoice = actives[0]
    else:
        # create new invoice
        invoice = Invoice.objects.create(
                client=proforma.client)

    if not proforma.id:
        pf = Proforma.objects.create(
            user=proforma.user,
            shop=proforma.shop,
            description=proforma.description,
            client=proforma.client,
            discount=proforma.discount)

        for item in line_items:
            line = pf.invoice_lines.create(
                invoice=invoice,
                article=item.article,
                quantity=item.quantity)

            transfer_flow(
                obj=pf, 
                article=item.article, 
                quantity=item.quantity, 
                source=pf.shop, 
                destiny=pf.client, 
                cost=item.article.cost, 
                price=line.price())

            tr_list.append({'account': pf.shop.get_account_name(),'amount': -item.total()})
            tr_list.append({'account': pf.client.get_account_name(), 'amount': item.total()})
            transacction_description = 'compra de productos'

        txn = create_transacction(
            user=proforma.user, 
            description=transacction_description, 
            invoice=invoice, 
            inputs=tr_list)

        print '**** Proforma creada con exito ****'
    # edit existing quantity items
    else:
        pf = Proforma.objects.get(id=proforma.id)

        pf.status = proforma.status
        pf.save() 

        for new_line in line_items:
            old_line = InvoiceLine.objects.get(id=new_line.id)
            qty_old = old_line.quantity
            diff = qty_old - new_line.quantity

            old_line.quantity = new_line.quantity
            old_line.save()

            if diff > 0:
                # returned products
                src_obj = pf.client
                dest_obj = pf.shop
                transacction_description = 'productos retornados'

                transfer_flow(
                    obj=pf, 
                    article=old_line.article, 
                    quantity=abs(diff), 
                    source=pf.client, 
                    destiny=pf.shop, 
                    cost=old_line.article.cost, 
                    price=old_line.price())

            elif diff < 0:
                # added prodcuts
                src_obj = pf.shop
                dest_obj = pf.client
                transacction_description = 'incremento de productos'

                transfer_flow(
                    obj=pf, 
                    article=old_line.article, 
                    quantity=abs(diff), 
                    source=pf.shop, 
                    destiny=pf.client, 
                    cost=old_line.article.cost, 
                    price=old_line.price())

            else:
                # without changes in quantity
                pass


            if diff != 0:
                tr_list.append({
                    'account': src_obj.get_account_name(),
                    'amount': - abs(diff) * old_line.price()
                    })
                tr_list.append({
                    'account': dest_obj.get_account_name(), 
                    'amount': abs(diff) * old_line.price()
                    })

        if len(tr_list) > 0:
            txn = create_transacction(
                user=proforma.user, 
                description=transacction_description, 
                invoice=invoice, 
                inputs=tr_list)

            print '**** Proforma actualizada con exito ****'
        else:
            print '**** Proforma salvada ****'
        

@transaction.atomic
def pay_invoice(invoice, user, payment_list):
    """
    create payment for actual invoice
    """
    for payment in payment_list:
        pay = invoice.payments.create(
            amount=payment.amount, 
            detail=payment.detail)

        txn = create_transacction(
            user=user, 
            description=pay.detail, 
            invoice=invoice, 
            inputs=[
                {
                    'account': invoice.client.get_account_name(),
                    'amount': - pay.amount
                },
                {
                    'account': pay.get_account_name(),
                    'amount': pay.amount
                }
                ])
        print txn

    if invoice.client.has_discounts():
        process_discounts()

    if invoice.client.has_bonus():
        process_bonus()

    """
    check if invoice is payed
    when total_debt is 0 or negative (funds)
    """
    if not invoice.total_debt() > 0:
        if invoice.active is True:
            invoice.active = False
            invoice.save()


def process_discounts():
    """
    process payment invoice for apply discounts
    """
    pass

def process_bonus():
    """
    process payment invoice for apply bonus
    """
    pass