# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventarios', '0002_auto_20151029_1242'),
    ]

    operations = [
        migrations.AlterField(
            model_name='flow',
            name='object_id',
            field=models.CharField(max_length=32, null=True),
        ),
    ]
