# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventarios', '0003_auto_20151029_1516'),
    ]

    operations = [
        migrations.AlterField(
            model_name='flow',
            name='object_id',
            field=models.CharField(max_length=64, null=True),
        ),
    ]
