# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventarios', '0004_auto_20151029_1517'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='flow',
            options={'ordering': ['cdate'], 'verbose_name': 'Movimiento Inventario', 'verbose_name_plural': 'Movimientos Inventario'},
        ),
    ]
