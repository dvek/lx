# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('productos', '0001_initial'),
        ('contenttypes', '0002_remove_content_type_name'),
        ('clientes', '0001_initial'),
        ('ubicacion', '0001_initial'),
        ('proveedores', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Flow',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('object_id', models.PositiveIntegerField()),
                ('quantity', models.IntegerField(default=0, verbose_name='Cantidad')),
                ('cost', models.DecimalField(verbose_name='Costo', max_digits=12, decimal_places=4)),
                ('price', models.DecimalField(verbose_name='Precio', max_digits=12, decimal_places=4)),
                ('article', models.ForeignKey(verbose_name='Producto', to='productos.Article')),
                ('client', models.ForeignKey(verbose_name='Cliente', to='clientes.Client', null=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('provider', models.ForeignKey(verbose_name='Proveedor', to='proveedores.Provider', null=True)),
                ('shop', models.ForeignKey(verbose_name='Almac\xe9n', to='ubicacion.Shop', null=True)),
            ],
            options={
                'verbose_name': 'Movimiento Inventario',
                'verbose_name_plural': 'Movimientos Inventario',
            },
        ),
    ]
