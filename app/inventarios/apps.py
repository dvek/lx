from django.apps import AppConfig

class InventarioConfig(AppConfig):
    name = 'inventarios'
    
    def ready(self):
        """
        set time zones dict
        """
        import receivers