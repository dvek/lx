#!/usr/bin/python 
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from lib.models import TrackingMixin


class Flow(TrackingMixin):

    content_type = models.ForeignKey(ContentType)
    object_id = models.CharField(max_length=64, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    shop = models.ForeignKey('ubicacion.Shop', null=True, verbose_name=u'Almacén')
    client = models.ForeignKey('clientes.Client', null=True, verbose_name=u'Cliente')
    provider = models.ForeignKey('proveedores.Provider', null=True, verbose_name=u'Proveedor')
    article = models.ForeignKey('productos.Article', verbose_name=u'Producto')
    quantity = models.IntegerField(default=0, verbose_name=u'Cantidad')
    cost = models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Costo')
    price = models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Precio')
    # encargado = models.ForeignKey('encargado.Encargado',verbose_name=u'Encargado')

    class Meta:
        verbose_name = u'Movimiento Inventario'
        verbose_name_plural = u'Movimientos Inventario'
        ordering = ['cdate']

    def __unicode__(self):
        return u'%s %s %s' % (self.content_object, self.article, self.quantity)

    def total_price(self):
        return (self.quantity * self.price)

    def total_cost(self):
        return (abs(self.quantity) * self.cost)

    def source(self):
        if self.quantity < 0:
            if self.shop:
                return self.shop.name
            if self.client:
                return self.client
            return self.provider
        return ' -- '
    source.short_description = u'Origen'

    def destiny(self):
        if self.quantity > 0:
            if self.shop:
                return self.shop.name
            if self.client:
                return self.client
            return self.provider
        return ' -- '
    destiny.short_description = u'Destino'
