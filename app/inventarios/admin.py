from django.contrib import admin

from models import Flow


@admin.register(Flow)
class FlowAdmin(admin.ModelAdmin):
	list_display = ('cdate', 'content_object', 'source', 'destiny', 'quantity')
