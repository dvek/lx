from django.db import models, transaction
from models import Flow
from ubicacion.models import Shop
from clientes.models import Client
from proveedores.models import Provider

@transaction.atomic
def transfer_flow(obj, article, quantity, source, destiny, cost, price):
    # output flow
    Flow.objects.create(
        content_object=obj,
        shop=source if isinstance(source, Shop) else None,
        client=source if isinstance(source, Client) else None,
        provider=source if isinstance(source, Provider) else None,
        article=article,
        quantity=-quantity,
        cost=cost,
        price=price)

    # input flow
    Flow.objects.create(
        content_object=obj,
        shop=destiny if isinstance(destiny, Shop) else None,
        client=destiny if isinstance(destiny, Client) else None,
        provider=destiny if isinstance(destiny, Provider) else None,
        article=article,
        quantity=quantity,
        cost=cost,
        price=price)

    if isinstance(source, Shop):
        article.qty_stock -= quantity
        article.save()

    if isinstance(destiny, Shop):
        article.qty_stock += quantity
        article.save()
