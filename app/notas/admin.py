#!/usr/bin/python 
# -*- coding: utf-8 -*-

from django.contrib import admin, messages
from models import InputNote

@admin.register(InputNote)
class InputNoteAdmin(admin.ModelAdmin):
    list_display = ('number', 'imported', 'cdate', 'activo', 'dui', 'provider', 
        'shop', 'csv_file','description', 'total')
    list_filter = ('number', 'cdate')
    fields = ('number', 'cdate', 'activo', 'dui', 'provider', 'shop',
        'description', 'csv_file', 'total', )
    readonly_fields = ('imported','number', 'total', 'cdate')

    def get_readonly_fields(self, request, obj=None):
        # adicionar una tupla de campos de solo lectura
        if obj and obj.imported:
            return self.readonly_fields + ('number', 'imported', 'date', 'activo', 'dui', 'provider', 'shop',
        'description', 'csv_file', 'total', 'ex_rate')
        return self.readonly_fields

    def has_delete_permission(self, request, obj=None):
        if obj and obj.imported:
            return False
        return True