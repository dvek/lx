# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('ubicacion', '0002_auto_20151026_0115'),
        ('proveedores', '0002_auto_20151026_0115'),
    ]

    operations = [
        migrations.CreateModel(
            name='InputNote',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n')),
                ('udate', models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n')),
                ('number', models.CharField(max_length=12, unique=True, null=True, verbose_name='N\xba Nota', blank=True)),
                ('imported', models.BooleanField(default=False, verbose_name=b'Importado')),
                ('activo', models.BooleanField(default=True, verbose_name='Activo')),
                ('dui', models.CharField(max_length=20, verbose_name='N\xba DUI')),
                ('description', models.TextField(verbose_name='Descripci\xf3n')),
                ('total', models.DecimalField(default=b'0.0000', verbose_name='Costo Total', max_digits=12, decimal_places=4)),
                ('ex_rate', models.DecimalField(default=b'0.00', verbose_name='Tipo de Cambio', max_digits=12, decimal_places=2)),
                ('csv_file', models.FileField(upload_to=b'importados/%Y/%m/%d', verbose_name='Archivo CSV')),
                ('provider', models.ForeignKey(verbose_name='Proveedor', to='proveedores.Provider')),
                ('shop', models.ForeignKey(verbose_name='Almacen', to='ubicacion.Shop')),
            ],
            options={
                'verbose_name': 'Nota de Ingreso de Mercader\xeda',
                'verbose_name_plural': 'Notas de Ingreso de Mercader\xedas',
            },
        ),
    ]
