#!/usr/bin/python 
# -*- coding: utf-8 -*-

import csv
from django.db import models, transaction
from django.conf import settings
from django.utils import timezone
from django.forms.models import ValidationError
from decimal import Decimal
from lib.models import TrackingMixin
from productos.models import Article
from inventarios.utils import transfer_flow


class InputNote(TrackingMixin):
    number = models.CharField(max_length=12, null=True, blank=True, unique=True, verbose_name=u'Nº Nota')
    imported = models.BooleanField(default=False, verbose_name='Importado')
    activo = models.BooleanField(default=True, verbose_name=u'Activo')
    dui = models.CharField(max_length=20, verbose_name=u'Nº DUI')
    provider = models.ForeignKey('proveedores.Provider', verbose_name=u'Proveedor')
    shop = models.ForeignKey('ubicacion.Shop', verbose_name=u'Almacen')
    description = models.TextField(verbose_name=u'Descripción')
    total = models.DecimalField(max_digits=12, decimal_places=4, default='0.0000', verbose_name=u'Costo Total')
    ex_rate = models.DecimalField(max_digits=12, decimal_places=2, default='0.00', verbose_name=u'Tipo de Cambio')
    csv_file = models.FileField(upload_to='importados/%Y/%m/%d', verbose_name=u'Archivo CSV')

    class Meta:
        verbose_name = u'Nota de Ingreso de Mercadería'
        verbose_name_plural = u'Notas de Ingreso de Mercaderías'

    def __unicode__(self):
        return u'Nota de Ingreso: %s' % (self.number)

    def save(self, *args, **kwargs):
        super(InputNote, self).save(*args, **kwargs)

        filename = '%s%s' % (settings.PROJECT_DIR, self.csv_file.url)
        with open(filename, 'rU') as f:
            self.total = self.import_csv(f)
            self.imported = True
            super(InputNote, self).save(*args, **kwargs)

    @transaction.atomic
    def import_csv(self, filecsv):
        reader = csv.reader(filecsv, delimiter=';', dialect='excel')
        costo_total = 0
        # ignorar la primera fila son el nombre de los campos
        reader.next()
        try:
            for row in reader:
                print row
                articulo, nuevo = Article.objects.get_or_create(code=row[1],
                defaults={'branch': row[0],
                'description': row[2], 
                'name': row[3], 
                'reference': row[4],
                'size': row[5],
                'color': row[6], 
                'branch_code': row[7],
                'cost': Decimal(row[8].replace(',','.').strip()),
                'dist_price': Decimal(row[9].replace(',','.').strip()),
                'public_price': Decimal(row[10].replace(',','.').strip())})
                
                if not nuevo:
                    articulo.branch = row[0]
                    articulo.description = row[2]
                    articulo.name = row[3]
                    articulo.reference = row[4]
                    articulo.size = row[5]
                    articulo.color = row[6]
                    articulo.branch_code = row[7]
                    articulo.cost = Decimal(row[8].replace(',','.').strip())
                    articulo.dist_price = Decimal(row[9].replace(',','.').strip())
                    articulo.public_price = Decimal(row[10].replace(',','.').strip())
                    articulo.save()
                    print '--- fila actualizada ---'
                else:
                    print '--- fila creada ---'

                costo_total += articulo.cost * int(row[11].strip())

                print 'antes del ingreso del inventario'
                print Decimal(row[10].replace(',','.').strip())
                transfer_flow(
                    obj=self, 
                    article=articulo, 
                    quantity=int(row[11].strip()), 
                    source=self.provider, 
                    destiny=self.shop, 
                    cost=Decimal(row[8].replace(',','.').strip()), 
                    price=Decimal(row[10].replace(',','.').strip())
                )

            print '--- fin de la importacion ---'
                
        except csv.Error as csv_error:
            print csv_error
            raise ValidationError(u'Error al procesar el archivo csv')

        return costo_total
