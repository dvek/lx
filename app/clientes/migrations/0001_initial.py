# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('ubicacion', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=250, verbose_name='Cliente')),
                ('nit', models.CharField(max_length=20, verbose_name=b'NIT')),
                ('address', models.TextField(verbose_name=b'Direccion')),
                ('phone', models.CharField(max_length=20, verbose_name=b'Telefono')),
                ('mobile', models.CharField(max_length=20, verbose_name=b'Celular')),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('city', models.ForeignKey(verbose_name='Ciudad', to='ubicacion.City')),
            ],
            options={
                'verbose_name': 'Cliente',
                'verbose_name_plural': 'Clientes',
            },
        ),
        migrations.CreateModel(
            name='ClientType',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=150, verbose_name='Nombre')),
                ('slug', models.SlugField(null=True, verbose_name='abr.', blank=True)),
                ('discount', models.BooleanField(default=False, verbose_name='Descuento')),
                ('credit', models.BooleanField(default=False, verbose_name=b'Credito')),
                ('days_term', models.IntegerField(default=0, verbose_name='Dias plazo')),
            ],
            options={
                'verbose_name': 'Tipo cliente',
                'verbose_name_plural': 'Tipos cliente',
            },
        ),
        migrations.CreateModel(
            name='ClientWallet',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=150, verbose_name='Nombre')),
            ],
            options={
                'verbose_name': 'Cartera cliente',
                'verbose_name_plural': 'Carteras cliente',
            },
        ),
        migrations.AddField(
            model_name='client',
            name='client_type',
            field=models.ForeignKey(verbose_name='Tipo cliente', to='clientes.ClientType'),
        ),
        migrations.AddField(
            model_name='client',
            name='state',
            field=models.ForeignKey(verbose_name='Departamento', to='ubicacion.State'),
        ),
        migrations.AddField(
            model_name='client',
            name='wallet',
            field=models.ForeignKey(verbose_name=b'Cartera', to='clientes.ClientWallet'),
        ),
    ]
