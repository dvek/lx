from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from models import Client


@receiver(post_save, sender=Client, dispatch_uid='check_account_client')
def check_account_client(sender, instance, created, **kwargs):
    """
    verifica que exista cuenta para el cliente
    de otra forma crea una nueva
    """
    if created:
        instance.get_account()