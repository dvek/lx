#!/usr/bin/python 
# -*- coding: utf-8 -*-

from django.db import models
from lib.models import TrackingMixin, AccountMixin


class ClientType(TrackingMixin):
    """
    Tipo de Cliente si tiene credito, descuento, plazo en dias
    """
    name = models.CharField(max_length=150, verbose_name=u'Nombre')
    slug = models.SlugField(verbose_name=u'abr.', null=True, blank=True)
    discount = models.BooleanField(default=False, verbose_name=u'Descuento')
    credit = models.BooleanField(default=False, verbose_name='Credito')
    days_term = models.IntegerField(default=0, verbose_name=u'Dias plazo')

    class Meta:
        verbose_name = u'Tipo cliente'
        verbose_name_plural = u'Tipos cliente'

    def __unicode__(self):
        return u'%s (Descuento: %s, Credito: %s, Dias plazo: %s)' % \
         (self.name, self.discount, self.credit, self.days_term)


class ClientWallet(TrackingMixin):
    """
    Cartera de client, solo nominal
    """
    name = models.CharField(max_length=150, verbose_name=u'Nombre')

    class Meta:
        verbose_name = u'Cartera cliente'
        verbose_name_plural = u'Carteras cliente'

    def __unicode__(self):
        return u'%s' % self.name


class Client(TrackingMixin, AccountMixin):
    """
    representacion del cliente y sus propiedades
    """
    name = models.CharField(max_length=250, verbose_name=u'Cliente')
    nit = models.CharField(max_length=20, verbose_name='NIT')
    wallet = models.ForeignKey(ClientWallet, verbose_name='Cartera')
    client_type = models.ForeignKey(ClientType, verbose_name=u'Tipo cliente')

    state = models.ForeignKey('ubicacion.State', verbose_name=u'Departamento')
    city = models.ForeignKey('ubicacion.City', verbose_name=u'Ciudad')
    address = models.TextField(verbose_name='Direccion')
    phone = models.CharField(max_length=20, verbose_name='Telefono')
    mobile = models.CharField(max_length=20, verbose_name='Celular')
    email = models.EmailField(verbose_name=u'Email')    

    class Meta:
        verbose_name = u'Cliente'
        verbose_name_plural = u'Clientes'

    @staticmethod   
    def autocomplete_search_fields():
        return ('id__iexact', 'name__icontains',)

    def __unicode__(self):
        return u'%s' % self.name

    def has_discounts(self):
        if self.client_type.credit and self.client_type.discount:
            return True
        return False

    def has_bonus(self):
        if self.client_type.credit:
            return True
        return self.client_type
