from django.contrib import admin

from models import ClientType, ClientWallet, Client

@admin.register(ClientType)
class ClienTypeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name', )}


@admin.register(ClientWallet)
class ClientWalletAdmin(admin.ModelAdmin):
    pass


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'nit', 'client_type', 'email',)
