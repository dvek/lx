#!/usr/bin/python 
# -*- coding: utf-8 -*-

from django.db import models
from lib.models import TrackingMixin, AccountMixin

class Provider(TrackingMixin, AccountMixin):

    name = models.CharField(max_length=150, verbose_name=u'Nombre proveedor')

    class Meta:
        verbose_name = u'Proveedor'
        verbose_name_plural = u'Proveedores'

    def __unicode__(self):
        return u'%s' % self.name
