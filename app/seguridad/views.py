from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import View, TemplateView, DetailView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm


class HomeView(TemplateView):
	template_name = 'home.html'


class LoginView(View):
    def render_form(self, request):
        form = AuthenticationForm(request)
        return render(request, 'seguridad/login.html', {'form':form})

    def get(self, request):
        form = AuthenticationForm(request)
        return self.render_form(request)

    def post(self, request):
        """Login user, success goes to root of app"""
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            #TODO error messages for failed login
            return self.render_form(request)

class LogoutView(View):
    def get(self, request):
        if request.user:
            logout(request)
        return HttpResponseRedirect('/login/')