#!/usr/bin/python 
# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings
from django.contrib.auth.models import Group, AbstractBaseUser, PermissionsMixin
from django.utils import timezone

from lib.models import TrackingMixin
from managers import MyUserManager


class MyUser(AbstractBaseUser, PermissionsMixin):
    """
    representacion de los usuarios y sus prepiedades
    """
    username = models.CharField(max_length=50, unique=True, verbose_name=u'Usuario')
    email = models.EmailField(max_length=254, unique=True, verbose_name=u'Correo electronico')
    first_name = models.CharField(max_length=100, blank=True, null=True, verbose_name=u'Nombe')
    middle_name = models.CharField(max_length=150, blank=True, null=True, verbose_name=u'Ap. Paterno')
    last_name = models.CharField(max_length=150, blank=True, null=True, verbose_name=u'Ap. Materno')
    phone = models.CharField(max_length=15, blank=True, null=True, verbose_name=u'Telefono')
    mobile = models.CharField(max_length=15, blank=True, null=True, verbose_name=u'Celular')
    is_active = models.BooleanField(default=True, verbose_name=u'Es activo')
    is_staff = models.BooleanField(default=False, verbose_name=u'Es staff')
    date_joined = models.DateTimeField(default=timezone.now, verbose_name=u'Fecha de registro')

    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = u'Usuario'
        verbose_name_plural = u'Usuarios'

    def __unicode__(self):
        return u'%s' % self.username

    def get_short_name(self):
        return u'%s' % self.username


class MyGroup(Group):
    """
    representacion de los grupos
    """
    TYPES = [
        ('admins', 'Administradores'),
        ('users', 'Usuarios'),
    ]

    type = models.CharField(max_length=15, default='users', choices=TYPES, verbose_name=u'Tipo')
    members = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, verbose_name=u'Miembros')
    is_active = models.BooleanField(default=True, verbose_name='Activo')

    class Meta:
        verbose_name = u'Grupo'
        verbose_name_plural = u'Grupos'

    def __unicode__(self):
        return u'%s' % self.name