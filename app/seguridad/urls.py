
from django.conf.urls import patterns, url
from django.views.decorators.csrf import csrf_exempt

from views import HomeView, LoginView, LogoutView


urlpatterns = patterns(
    '',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'login/', LoginView.as_view(), name='login'),
    url(r'logout/', LogoutView.as_view(), name='logout'),
)
