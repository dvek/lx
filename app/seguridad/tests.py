from django.test import TestCase
from models import MyGroup, MyUser


class MyUserTest(TestCase):

    def setUp(self):
        pass

    def test_attr_username(self):
        user = MyUser.objects.create(username='user1', email='user1@mail.com')
        self.assertEqual(user.username, 'user1')

    def test_attr_email(self):
        user = MyUser.objects.create(username='user1', email='user1@mail.com')
        self.assertEqual(user.email, 'user1@mail.com')

    def test_attr_is_active(self):
        user = MyUser.objects.create(username='user1', email='user1@mail.com', is_active=True)
        self.assertTrue(user.is_active)

    def test_attr_is_staff(self):
        user = MyUser.objects.create(username='user1', email='user1@mail.com', is_staff=True)
        self.assertTrue(user.is_staff)


class MyGroupTest(TestCase):

    def setUp(self):
        pass

    def test_attr_name(self):
        group = MyGroup.objects.create(name='group1')
        self.assertEqual(group.name, 'group1')

    def test_attr_is_active(self):
        group = MyGroup.objects.create(name='group1', is_active=True)
        self.assertEqual(group.is_active, True)

    def test_attr_members(self):
        user = MyUser.objects.create(username='user', email='user@gmail.com')
        group = MyGroup.objects.create(name='group1', is_active=True)
        group.members.add(user)
        self.assertEqual(user, group.members.all()[0])
