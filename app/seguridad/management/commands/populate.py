from django.core.management.base import BaseCommand
from seguridad.models import MyUser, MyGroup
from ubicacion.models import State, City, Branch, Shop
from clientes.models import ClientType, ClientWallet, Client
from productos.models import Unity, Article, UpdateStock


class Command(BaseCommand):
    help = 'datos basicos para el sistema'

    def handle(self, *args, **options):
        print 'populate db...'
        
        self._generate_groups()
        self._generate_users()
        self._generate_locations()
        self._generate_clients()
        self._generate_articles()

    def _generate_groups(self):
        print 'generating groups...'
        MyGroup.objects.all().delete()
        
        self.admin = MyGroup.objects.create(name='Administradores', type='admins')
        self.others = MyGroup.objects.create(name='Encargados', type='users')

    def _generate_users(self):
        print 'generating users...'
        MyUser.objects.all().delete()

        root = MyUser.objects.create_superuser(
            username='admin',
            email='denieru@gmail.com',
            password='admin')

        user1 = MyUser.objects.create(
            username='luc', 
            email='luc@hanantek.com',
            password='luc2015',
            is_staff=True)

        user2 = MyUser.objects.create(
            username='erika', 
            email='erika@hanantek.com',
            password='erika2015',
            is_staff=True)

        self.admin.members = [user1, user2]

    def _generate_locations(self):
        print 'generating locations...'
        State.objects.all().delete()
        City.objects.all().delete()
        Branch.objects.all().delete()
        Shop.objects.all().delete()

        self.state = State.objects.create(name='La Paz')
        self.city = City.objects.create(name='La Paz')
        Branch.objects.create(name='Central')
        Shop.objects.create(
            name='Central', 
            state=self.state, 
            city=self.city,
            location='ubication de la tienda',
            phone='111 111 111')

    def _generate_clients(self):
        print 'generating clients...'
        ClientType.objects.all().delete()
        ClientWallet.objects.all().delete()
        Client.objects.all().delete()

        pay = ClientType.objects.create(
            name='Contado',
            discount=False,
            credit=False,
            days_term=0)

        credit = ClientType.objects.create(
            name='Credito',
            discount=True,
            credit=True,
            days_term=15)

        wallet = ClientWallet.objects.create(name='Cartera 1')

        client = Client.objects.create(
            name='Cliente contado',
            nit='111 222 333',
            wallet=wallet,
            client_type=pay,
            state=self.state,
            city=self.city,
            address='La Paz',
            phone='111 222 333',
            mobile='777 000 000',
            email='client@hanantek.com')

        client_credit = Client.objects.create(
            name='Cliente credito',
            nit='444 555 666',
            wallet=wallet,
            client_type=credit,
            state=self.state,
            city=self.city,
            address='La Paz',
            phone='111 222 333',
            mobile='777 000 000',
            email='credit@hanantek.com')

    def _generate_articles(self):
        print 'generating articles...'
        Unity.objects.all().delete()
        Article.objects.all().delete()
        UpdateStock.objects.all().delete()

        unit = Unity.objects.create(name='unidad', slug='u')
        article = Article.objects.create(
            name='Articulo generico',
            code='000',
            description='producto de prueba',
            reference='Otros',
            size='XL',
            branch='BigBranch',
            color='White',
            branch_code='001',
            unit=unit,
            dist_price=100,
            public_price=80,
            cost=50,
            )
