from models import MyUser, MyGroup

def has_su_perm(user):
    if user.is_superuser:
        return True
    return False

def has_admin_perm(user):
    return user.groups.filter(name='admins').exists() | user.is_superuser
    
def has_user_perm(user):
    return user.groups.filter(name='admins').exists() | \
        user.groups.filter(name='users').exists() | user.is_superuser
