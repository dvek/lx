# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('seguridad', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='mygroup',
            name='type',
            field=models.CharField(default=b'users', max_length=15, verbose_name='Tipo', choices=[(b'admins', b'Administradores'), (b'users', b'Usuarios')]),
        ),
    ]
