# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='MyUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(unique=True, max_length=50, verbose_name='Usuario')),
                ('email', models.EmailField(unique=True, max_length=254, verbose_name='Correo electronico')),
                ('first_name', models.CharField(max_length=100, null=True, verbose_name='Nombe', blank=True)),
                ('middle_name', models.CharField(max_length=150, null=True, verbose_name='Ap. Paterno', blank=True)),
                ('last_name', models.CharField(max_length=150, null=True, verbose_name='Ap. Materno', blank=True)),
                ('phone', models.CharField(max_length=15, null=True, verbose_name='Telefono', blank=True)),
                ('mobile', models.CharField(max_length=15, null=True, verbose_name='Celular', blank=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='Es activo')),
                ('is_staff', models.BooleanField(default=False, verbose_name='Es staff')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Fecha de registro')),
            ],
            options={
                'verbose_name': 'Usuario',
                'verbose_name_plural': 'Usuarios',
            },
        ),
        migrations.CreateModel(
            name='MyGroup',
            fields=[
                ('group_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='auth.Group')),
                ('is_active', models.BooleanField(default=True, verbose_name=b'Activo')),
                ('members', models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='Miembros')),
            ],
            options={
                'verbose_name': 'Grupo',
                'verbose_name_plural': 'Grupos',
            },
            bases=('auth.group',),
        ),
        migrations.AddField(
            model_name='myuser',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='myuser',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
    ]
