# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('seguridad', '0002_mygroup_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mygroup',
            name='members',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='Miembros', blank=True),
        ),
    ]
