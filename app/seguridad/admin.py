from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _

from models import MyUser, MyGroup
from forms import EncargadoCreationForm, EncargadoChangeForm


admin.site.unregister(Group)

@admin.register(MyUser)
class MyUserAdmin(UserAdmin):
	fieldsets = (
		(None,  {'fields': ('username', 'email','password')}),
		(_('Personal info'), {'fields': 
			('first_name', 'middle_name', 'last_name', 
				'phone', 'mobile')}),
		(_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
			'groups', 'user_permissions')}),
		(_('Important dates'), {'fields': ('last_login', 'date_joined')}),
		)
	add_fieldsets = (
		(None, {
			'classes': ('wide',),
			'fields': ('username', 'email', 'password1', 'password2')
			}),
		(_('Personal info'), {'fields':
			('first_name', 'middle_name', 'last_name',
				'phone', 'mobile')})
	)

	form = EncargadoChangeForm
	add_form = EncargadoCreationForm
	list_display = ('username', 'email', 'first_name', 'middle_name', 'last_name','is_staff')
	search_fields = ('username', 'email', 'first_name', 'middle_name', 'last_name')


@admin.register(MyGroup)
class MyGroupAdmin(GroupAdmin):
	pass
