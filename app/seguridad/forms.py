from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from models import MyUser

class EncargadoCreationForm(forms.ModelForm):
    """
    Un formulario que crea un usuario, sin privilegios, con el email y password proporcionados
    """
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = MyUser
        fields = ('username', 'email')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords dont match')
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(EncargadoCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

class EncargadoChangeForm(forms.ModelForm):
    """
    Un formulario para actualizar usuarios, incluye todos los campos del usuario,
    pero reemplaza el campo de password con el campo de password hash del admin
    """

    password = ReadOnlyPasswordHashField(label= ("Password"),
        help_text= ("Para cambiar password use <a href=\"password/\">este formulario</a>."))

    class Meta:
        model = MyUser
        fields = ('username', 'email', 'password', 'is_staff', 'is_superuser')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial['password']