# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('productos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='article',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='unity',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='unity',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='updatestock',
            name='cdate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='updatestock',
            name='udate',
            field=models.DateTimeField(auto_now=True, verbose_name='Actualizaci\xf3n'),
        ),
    ]
