# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('ubicacion', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=150, verbose_name='Articulo')),
                ('code', models.CharField(unique=True, max_length=30, verbose_name='Codigo')),
                ('description', models.CharField(max_length=254, verbose_name='Descripcion')),
                ('reference', models.CharField(max_length=20, verbose_name='Referencia')),
                ('size', models.CharField(max_length=30, verbose_name='Tama\xf1o')),
                ('branch', models.CharField(max_length=150, verbose_name='Marca')),
                ('color', models.CharField(max_length=30, verbose_name='Color')),
                ('branch_code', models.CharField(max_length=9, verbose_name='Codigo marca')),
                ('dist_price', models.DecimalField(verbose_name='Precio distribuidor', max_digits=12, decimal_places=4)),
                ('public_price', models.DecimalField(verbose_name='Precio publico', max_digits=12, decimal_places=4)),
                ('cost', models.DecimalField(verbose_name='Costo', max_digits=12, decimal_places=4)),
                ('qty_stock', models.IntegerField(default=0, verbose_name='Cantidad disponible')),
                ('qty_loan', models.IntegerField(default=0, verbose_name='Cantidad prestada')),
                ('stock', models.BooleanField(default=0, verbose_name='En stock')),
                ('loan', models.BooleanField(default=0, verbose_name='En Prestamo')),
                ('active', models.BooleanField(default=True, verbose_name='Activo')),
            ],
            options={
                'verbose_name': 'Articulo',
                'verbose_name_plural': 'Articulos',
            },
        ),
        migrations.CreateModel(
            name='Unity',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=150, verbose_name='Nombre')),
                ('slug', models.SlugField(null=True, verbose_name='abr.', blank=True)),
            ],
            options={
                'verbose_name': 'Unidad',
                'verbose_name_plural': 'Unidades',
            },
        ),
        migrations.CreateModel(
            name='UpdateStock',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('udate', models.DateTimeField(auto_now=True)),
                ('move_type', models.CharField(default=b'I', max_length=1, verbose_name='Tipo Movimiento', choices=[(b'I', b'Ingreso'), (b'O', b'Salida')])),
                ('description', models.CharField(max_length=254, verbose_name='Descripcion')),
                ('offset', models.IntegerField(default=0, verbose_name='Cantidad ajustada')),
                ('initial_stock', models.IntegerField(default=0, verbose_name='Stock inicial')),
                ('final_stock', models.IntegerField(default=0, verbose_name='Stock final')),
                ('article', models.ForeignKey(verbose_name='Articulo', to='productos.Article')),
                ('shop', models.ForeignKey(verbose_name='Tienda/Almacen', to='ubicacion.Shop')),
            ],
            options={
                'verbose_name': 'Movimiento de stock',
                'verbose_name_plural': 'Movimientos de stocks',
            },
        ),
        migrations.AddField(
            model_name='article',
            name='unit',
            field=models.ForeignKey(verbose_name='Unidad', to='productos.Unity', null=True),
        ),
    ]
