
from django.conf.urls import patterns, url
from django.views.decorators.csrf import csrf_exempt

from views import products


urlpatterns = patterns(
    '',
    url(r'products/(?P<uuid>[^/]+)/', csrf_exempt(products), name='products'),
)
