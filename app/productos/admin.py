from django.contrib import admin
from models import Unity, Article, UpdateStock


@admin.register(Unity)
class UnityAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    list_filter = ('name',)
    prepopulated_fields = {'slug': ('name', )}


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'description', 'reference', 'size', 'branch',
        'color', 'branch_code', 'dist_price', 'public_price', 'stock', 'loan', 'active')
    readonly_fields = ('qty_stock', 'qty_loan',)


@admin.register(UpdateStock)
class UpdateStockAdmin(admin.ModelAdmin):
    list_display = ('article', 'move_type', 'shop', 'offset', 'initial_stock', 'final_stock')

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.id is not None:
            return ('article', 'move_type', 'shop', 'description', 'offset', 'initial_stock', 'final_stock')
        return []

    def has_delete_permission(self, request, obj=None):
        return False




