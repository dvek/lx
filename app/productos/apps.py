from django.apps import AppConfig

class ProductoConfig(AppConfig):
    name = 'productos'
    
    def ready(self):
        """
        set time zones dict
        """
        from receivers import update_flows