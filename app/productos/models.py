#!/usr/bin/python 
# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone

from lib.models import TrackingMixin


class Unity(TrackingMixin):
    """
    unidades para los productos
    """
    name = models.CharField(max_length=150, verbose_name=u'Nombre')
    slug = models.SlugField(max_length=50, verbose_name=u'abr.', null=True, blank=True)

    class Meta:
        verbose_name = u'Unidad'
        verbose_name_plural = u'Unidades'

    def __unicode__(self):
        return u'%s' % self.name


class Article(TrackingMixin):
    """
    articulos/productos
    """
    name = models.CharField(max_length=150, verbose_name=u'Articulo')
    code = models.CharField(max_length=30, unique=True, verbose_name=u'Codigo')
    description = models.CharField(max_length=254, verbose_name=u'Descripcion')
    reference = models.CharField(max_length=20, verbose_name=u'Referencia')
    size = models.CharField(max_length=30, verbose_name=u'Tamaño')
    branch = models.CharField(max_length=150, verbose_name=u'Marca')
    color = models.CharField(max_length=30, verbose_name=u'Color')
    branch_code = models.CharField(max_length=9, verbose_name=u'Codigo marca')
    unit = models.ForeignKey(Unity, null=True, verbose_name=u'Unidad')

    dist_price = models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Precio distribuidor')
    public_price = models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Precio publico')
    cost = models.DecimalField(max_digits=12, decimal_places=4, verbose_name=u'Costo')

    qty_stock = models.IntegerField(default=0, verbose_name=u'Cantidad disponible')
    qty_loan = models.IntegerField(default=0, verbose_name=u'Cantidad prestada')

    stock = models.BooleanField(default=0, verbose_name=u'En stock')
    loan = models.BooleanField(default=0, verbose_name=u'En Prestamo')
    active = models.BooleanField(default=True, verbose_name=u'Activo')

    class Meta:
        verbose_name = u'Articulo'
        verbose_name_plural = u'Articulos'

    @staticmethod   
    def autocomplete_search_fields():
        return ('id__iexact', 'name__icontains', 'code__icontains','description__icontains')

    def __unicode__(self):
        return u'%s' % self.description

    def complete_description(self):
        return u'{0} {1} {2} {3} {4} {5}'.format(
            self.code,
            self.description,
            self.name,
            self.reference,
            self.size,
            self.color,
            self.branch_code)


class UpdateStock(TrackingMixin):
    """
    ajuste de inventarios
    """
    IN = 'I'
    OUT = 'O'
    MOVES = {
        (IN, 'Ingreso'),
        (OUT, 'Salida'),
    }

    article = models.ForeignKey(Article, verbose_name=u'Articulo')
    move_type = models.CharField(max_length=1, choices=MOVES, default=IN, verbose_name=u'Tipo Movimiento')
    shop = models.ForeignKey('ubicacion.Shop', verbose_name=u'Tienda/Almacen')
    description = models.CharField(max_length=254, verbose_name=u'Descripcion')
    offset = models.IntegerField(default=0, verbose_name=u'Cantidad ajustada')
    initial_stock = models.IntegerField(default=0, verbose_name=u'Stock inicial')
    final_stock = models.IntegerField(default=0, verbose_name=u'Stock final')

    class Meta:
        verbose_name = u'Movimiento de stock'
        verbose_name_plural = u'Movimientos de stocks'

    def __unicode__(self):
        return u'Ajuste stock %s %s' % (self.article, self.cdate)

    def save(self, *args, **kwargs):
        self._update_field_stock()
        super(UpdateStock, self).save(*args, **kwargs)

    def _update_field_stock(self):
        """
        actualizacion de los inventarios
        """
        self.initial_stock = self.article.qty_stock
        if self.move_type == self.IN:
            self.final_stock = self.initial_stock + self.offset
        else:
            self.final_stock = self.initial_stock - self.offset