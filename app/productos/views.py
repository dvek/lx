import json
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponse

from models import Article

def products(request, uuid):
    obj = get_object_or_404(Article, pk=uuid)
    data = {'id': str(obj.id), 
        'name': obj.name, 
        'public_price': str(obj.public_price), 
        'dist_price': str(obj.dist_price),
        'qty_stock': int(obj.qty_stock)}
    return HttpResponse(json.dumps(data), content_type = "application/json")

