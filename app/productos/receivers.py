from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from models import UpdateStock
from decimal import Decimal

@receiver(post_save, sender=UpdateStock)
def update_flows(sender, instance, **kwargs):
    """
    creacion de los movimientos de inventarios
    de ingreso desconocido (IN) hacia un almacen
    de salida de un almacen (OUT) hacia otro desconocido
    """
    from inventarios.utils import transfer_flow
    
    if instance.move_type == instance.IN:

        transfer_flow(
                obj=instance,
                article=instance.article,
                quantity=instance.offset,
                source=None,
                destiny=instance.shop,
                cost=instance.article.cost,
                price=instance.article.public_price)

    elif instance.move_type == instance.OUT:

        transfer_flow(
                obj=instance,
                article=instance.article,
                quantity=instance.quantity,
                source=instance.shop,
                destiny=None,
                cost=instance.article.cost,
                price=instance.article.public_price)

    else:
        pass